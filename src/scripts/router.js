import Vue from "vue";
import Router from "vue-router";

// import online from '@/pages/online/online'

Vue.use(Router);

// export default new Router({
const router = new Router({
    mode: "hash", //
    // mode: 'history',//后台支持路由,使用history.pushState和replaceState来管理记录
    // base: process.env.BASE_URL,
    routes: [{
            path: "/",
            name: "home",
            component: resolve => {
                require(["@/pages/home/home.vue"], resolve);
            },
            children: [{
                    path: "/",
                    name: "homeIndex",
                    component: resolve => {
                        require(["@/pages/homeIndex/homeIndex.vue"], resolve);
                    }
                },
                {
                    path: "infoForm",
                    name: "infoForm",
                    component: resolve => {
                        require(["@/pages/form/form.vue"], resolve);
                    }
                },
                {
                    path: "final",
                    name: "final",
                    component: resolve => {
                        require(["@/pages/final/final.vue"], resolve);
                    }
                }
            ]
        },
        {
            path: "/noMember",
            name: "noMember",
            component: resolve => {
                require(["@/pages/noMember/noMember.vue"], resolve);
            }
        },
        {
            path: "/online",
            name: "online",
            component: resolve => {
                require(["@/pages/online/online.vue"], resolve);
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    // console.log("router>>>>>to", to, " from>>>>", from);
    if (to.matched.length === 0) {
        from.name ? next({
            name: from.name
        }) : next("/"); //先尝试匹配本页的上一级，否则根路由
    } else {
        next(); //匹配到正确跳转
    }
});

export default router;